import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            int n;
            n = sc.nextInt();

            if (n == 1) {
                System.out.print("Please input number: ");
                int nn;
                nn = sc.nextInt();
                for (int i = 1; i <= nn; i++) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }

            } else if (n == 2) {
                System.out.print("Please input number: ");
                int nn;
                nn = sc.nextInt();
                for (int i = nn; i > 0; i--) {
                    for (int j = 0; j < i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            } else if (n == 3) {
                System.out.print("Please input number: ");
                int nn;
                nn = sc.nextInt();
                for (int i = 0; i < nn; i++) {
                    for (int j = 0; j < 5; j++) {
                        if (j >= i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            } else if (n == 4) {
                System.out.print("Please input number: ");
                int nn;
                nn = sc.nextInt();
                for (int i = nn; i >= 0; i--) {
                    for (int j = 0; j < 5; j++) {
                        if (j >= i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            } else if (n == 5) {
                System.out.println("Bye bye!!!");
            } else {
                System.out.println("Error: Please input number between 1-5 ");
            }

        }
    }
}

// sc.close();
